/* eslint-disable @next/next/no-page-custom-font */
import type { Metadata } from "next";
import { Inter } from "next/font/google";
//import "./globals.css";
import '@/common/styles/scss/main.scss'
//import { ScriptConstant } from "@/common/constants";
const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Great Ponton!",
  description:
    "Congratulations on progressing to the next stage of our application process for the Frontend Developer at Great Ponton!"
};

export default function RootLayout({
  children
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <head>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin={"anonymous"} />
        <link
          href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
          rel="stylesheet"
        />
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css"
        />
      </head>
      <body className={inter.className}>{children}</body>
      {/* <script src={ScriptConstant.MOBILE_FIRST} /> */}
    </html>
  );
}
