import React from "react";
import Card from "./Card/Card";
import TextInput from "@/common/components/textInput/TextInput";
import Button from "../../common/components/Button/Button";
import CheckBox from "@/common/components/checkBox/CheckBox";

const HomeWrapper = () => {
  return (
    <>
      <Card />
      <div className="form-information">
        <h1 className="font-semi-bold">Personal Information</h1>
        <div>
          <form className="tst-row">
            <div className="col-12 col-lg-9">
              <div className="tst-row py-5 gap-5">
                <div className="col-12 col-lg-6">
                  <TextInput label="First Name" isValid={false} />
                </div>
                <div className="col-12 col-lg-6">
                  <TextInput label="Last Name" isValid={false} />
                </div>
                <div className="col-12 col-lg-6">
                  <TextInput label="City" isValid={false} />
                </div>
                <div className="col-12 col-lg-6">
                  <TextInput label="Postal Code" isValid={false} />
                </div>
                <div className="col-12 col-lg-12">
                  <TextInput label="Address" isValid={false} />
                </div>
                <div className="col-12 col-lg-6">
                  <TextInput label="Email" isValid={true} />
                </div>
                <div className="col-12 col-lg-6">
                  <TextInput label="Phone" isValid={false} />
                </div>
                <div className="col-12 col-lg-12">
                  <TextInput type="password" label="Password" isValid={false} />
                </div>
                <div className="col-12 col-lg-12">
                  Use this email to log in to your
                  <a href="#" className="text-blue">
                    resumedone.io
                  </a>{" "}
                  account and receive notification
                </div>
                <Button title="Save" />
              </div>
            </div>
            <div className="col-12 col-lg-3 img-profil">
              <div className="tst-image">
                <img
                  src="/user.png"
                  alt=""
                />
              </div>
            </div>
          </form>
        </div>
        <div>
          <div className="tst-form">
             <CheckBox label={ <span>
                  Show my profile to serious employers on
                  <a href="#" className="text-blue">
                    hirethesbest.io
                  </a>{" "}
                  for free
                </span>} 
             />
          </div>

          <div className="tst-card p-4 d-flex flex-column gap-2 my-3">
            <h2 className="font-semi-bold font-lg">Delete account</h2>
            <div className="info font-normal">
              If you delete your account ,you'll be permanenty removing it from
              our systems - you can't undo it.
            </div>
            <div className="action cursor-pointer text-danger font-bold">
              Yes, Delete my account
            </div>
          </div>

          <div className="py-3">
            <div>
              <span className="text-blue">
                Get in touch with our support team
              </span>
              if you have any question or want to leave some feedback.
            </div>
            <div>We'll be happy to hear from you.</div>
          </div>
        </div>
      </div>
    </>
  );
};

export default HomeWrapper;
