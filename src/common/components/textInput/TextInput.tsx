import { FC } from "react";
import { Props } from "./props";

const TextInput:FC<Props> = ({label,isValid,type='text'}) => {
  return (
    <s-input>
      <input type={type} required className="pr-5" />
      <s-placeholder>{label}</s-placeholder>
      {
        isValid && (
            <s-prepend-icon>
             <i className="fa-solid fa-check"></i>
            </s-prepend-icon>
        )
      }
    </s-input>
  );
};

export default TextInput;
