import { FC } from "react"

const CheckBox:FC<Props> = ({label}) => {
  return (
    <div className="tst-checkbox">
              <label className="d-flex align-items-center">
                <input type="checkbox" />
                <span className="cr">
                  <i className="cr-icon fa-solid fa-check"></i>
                </span>
                {label && label}
              </label>
     </div>
  )
}

export default CheckBox