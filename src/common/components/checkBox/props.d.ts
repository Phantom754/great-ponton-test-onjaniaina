// export type  Props = {
//     label?:string
// }

import { ReactNode } from "react";

declare global {
    type Props = {
      label?:string | ReactNode
    };
}

export default Props