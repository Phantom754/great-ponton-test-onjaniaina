export type SvgProps = {
   path:string
   classNames?:string
   svgClassNames?:string
   height?:number
   width?: number
}