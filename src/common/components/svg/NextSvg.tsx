import { FC, PropsWithChildren } from "react"
import Image from 'next/image';
import { SvgProps } from "./svg.type"


const NextSvg:FC<PropsWithChildren<SvgProps>> = ({path,classNames,svgClassNames,width=32,height=32}) => {
  return (
    <Image
      priority
      src={path}
      height={height}
      width={width}
      alt="svg"
    />
  )
}

export default NextSvg