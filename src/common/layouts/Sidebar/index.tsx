"use client"
import { SvgConstant } from "@/common/constants";
import classNames from "classnames";
import React, { useState } from "react";
import { IoSettingsOutline } from "react-icons/io5";
import { PiClock } from "react-icons/pi";
import { RiArchiveDrawerLine } from "react-icons/ri"; 
import { menuList } from "./menuData";

const SideBar = () => {
  const [isActive,setActive] = useState<boolean>(false)
  const onToogleActive  = () => setActive(prev => !prev) 
  const [showMenu,setShowMenu] = useState<boolean>(false)
  const onToogleMenu  = () => setShowMenu(prev => !prev) 
  return (
    <>
      <div className="tst-side-mobile">
        <button id="hamburger-btn" onClick={onToogleMenu}>
          <i className="fa-solid fa-bars"></i>
        </button>
        <div className="logo">
          <div className="">
            <img alt="" src={SvgConstant.RESUME} />
          </div>
        </div>
      </div>
      <div className={classNames("tst-side" , { "active": showMenu })}>
        <div className="tst-side__header">
          <div className="tst-image">
            <img alt="" src={SvgConstant.RESUME} />
          </div>
        </div>
        <div className="tst-side__content">
          <ul>
            {
              menuList.map((menu,index) =>(
                <li key={index}>
                  <a href="#">
                    {menu.icon}
                    {menu.title}
                  </a>
                </li>
              ))
            }
            <li>
              <a href="#">
                <PiClock fontSize={29} />
                <span>Past search 1</span>
              </a>
            </li>
            <li>
              <a href="#">
                <PiClock fontSize={29} />
                <span>Past search 2</span>
              </a>
            </li>
            <li>
              <a href="#">
                <PiClock fontSize={29} />
                <span>Computers and information...</span>
              </a>
            </li>
            <li>
              <a href="#">
                <PiClock fontSize={29} />
                <span>Database Administrator</span>
              </a>
            </li>
            <li>
              <a href="#">
                <PiClock fontSize={29} />
                <span>Computer security</span>
              </a>
            </li>
            <li>
              <a href="#">
                <PiClock fontSize={29} />
                <span>Computer Systems Analyst</span>
              </a>
            </li>
            {/* board */}
            <li>
              <a href="#">
                <RiArchiveDrawerLine fontSize={29} />
                <span className="bold">My boards</span>
              </a>
              <div className="more" onClick={onToogleActive}>
                <i className="fa-solid fa-plus"></i>
              </div>
              <ul className={classNames("child" , { "active": isActive })}>
                <li>
                  <a href="#">
                    <img src="/board.png" alt="" />
                    <span>Board 1</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <img src="/board.png" alt="" />
                    <span>Board 2</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <img src="/board.png" alt="" />
                    <span>Board 3</span>
                  </a>
                </li>
              
                <li>
                  <a href="#">
                    <img src="/locks.png" alt="" />
                    <span>Board agent 1</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <img src="/locks.png" alt="" />
                    <span>Board agent 2</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <img src="/locks.png" alt="" />
                    <span>Board agent 3</span>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
        <div className="tst-side__footer">
          <div className="avatar">
            <div className="tst-image">
              <img src="./assets/img/profil-user.png" alt="" />
            </div>
            <div className="name">Carla</div>
          </div>
          <div className="conf">
            <IoSettingsOutline fontSize={30} /> 
          </div>
        </div>
      </div>
    </>
  );
};

export default SideBar;
