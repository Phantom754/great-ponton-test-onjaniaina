import { FileText, Search } from "lucide-react";
import { MdOutlineStar } from "react-icons/md";

export const menuList = [
   {
    icon:  <FileText />,
    title: <span className="bold">My template </span>
   },
   {
    icon: <Search />,
    title: <span className="bold">Search </span>
   },
   {
    icon: <MdOutlineStar fontSize={29} color="yellow" /> ,
    title: <span>Software Engineer</span>
   },
   {
    icon: <MdOutlineStar fontSize={29} color="yellow" />,
    title: <span>Computer hardware engineer</span>
   },
   {
    icon: <MdOutlineStar fontSize={29} color="yellow" />,
    title: <span>Network Engineer</span>
   },
   {
    icon: <MdOutlineStar fontSize={29} color="yellow" />,
    title: <span>Technical Support</span>
   },
   {
    icon: <MdOutlineStar fontSize={29} color="yellow" />,
    title: <span>Network administrator</span>
   },
   {
    icon: <MdOutlineStar fontSize={29} color="yellow" />,
    title: <span>Management</span>
   },
   {
    icon: <MdOutlineStar fontSize={29} color="yellow" />,
    title: <span>Data analysis</span>
   },
   {
    icon: <MdOutlineStar fontSize={29} color="yellow" />,
    title: <span>Computer technician</span>
   }
]
